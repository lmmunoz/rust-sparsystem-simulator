# Rust Sparsystem Simulator
Bank System (Sparsystem) for testing future Rust and ORACLE Database Cloud implementations in different projects not related.
The system (R·Sparsystem·S) will have functionality to show share prices, interest payment, applied taxes, etc.

「Interests」and「Dividends」are provided using a DISCRETE Approach based on USER INTERACTIONS.
The reason to use this Discrete Approach instead of the classic Continuous (temporal) Approach is because this is a Local Bank System with very limited user interactions.
This may cause a potential bankruptcy situation when there is absence of user interactions during long periods of time.

![](Z_Drawable/Captura_de_pantalla_de_2024-08-06_16-29-26.png/)

![](Z_Drawable/Captura_de_pantalla_de_2024-08-06_16-30-05.png/)

## Installation
To facilitate its implementation it lacks a GUI, instead it uses a simple Console UI. Just download the project and run it using Visual Studio.
