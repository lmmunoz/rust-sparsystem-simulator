const STACK:&str="|::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|";
const SLICE:&str="|------------------------------------------------------------------|";
const EMPTY:&str="|                                                                  |";
const LOWES:&str="____________________________________________________________________";
const LOWSQ:&str="|__________________________________________________________________|";
use std::io::{self, Read, Seek, Write};
use std::fs::OpenOptions;
use colored::Colorize;

fn xor_crypt(key: &[u8], data: &[u8]) -> Vec<u8> {
    data.iter().zip(key.iter().cycle()).map(|(d, k)| d ^ k)
    /*.filter(|&c| c.is_ascii_graphic() || c.is_ascii_whitespace()).filter(|&c| c != b'\n')*/
    .collect()}

  /////////////////////////////////
 /**/const LANGUAGE:&str="sv";////Available in "en" ENGLISH && "sv" SVENSKA
/////////////////////////////////
pub fn gs(key: &str) -> Option<&str> {
    let strings_file = match LANGUAGE {
        "en" => include_str!("strings_en.txt"),
        "sv" => include_str!("strings_sv.txt"),
        _ => panic!("Unsupported language"),
    };
    let strings: Vec<(&str, &str)> = strings_file
        .lines()
        .filter_map(|line| {
            let parts: Vec<&str> = line.splitn(2, '=').map(|s| s.trim()).collect();
            if parts.len() == 2 {
                Some((parts[0], parts[1]))
            } else {None}
        })
        .collect();
    strings.iter().find_map(|(k, v)| if *k == key { Some(*v) } else { None })
}

fn deposit(accus: &str) -> io::Result<()> {
    let mut file = OpenOptions::new().read(true).write(true).open("localdataccs.txt")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut balance=0;
    for line in contents.lines() {
        let uncrili=xor_crypt(b"9i2a9jr66d5e4f8h", line.as_bytes());
        let ununcrili=std::str::from_utf8(&uncrili).unwrap();
        let parts: Vec<&str> = ununcrili.split(";").collect();
        if parts.len() > 0 && parts[0] == accus {
            balance = parts[2].parse().expect("Failed getting balance");
            break;}}
    println!("{}",STACK.blue());
    println!("{}",STACK.blue());
    println!("{}",STACK.blue());
    let mut asmuchfill=String::new();
    let mut divisions = 0;
    while balance > 0 {balance /= 10;divisions += 1;}
    asmuchfill = ":".repeat(40-divisions);
    println!("|::{}{}::{}::|",asmuchfill,gs("balnotice").unwrap_or_default(),balance.to_string().blue());
    println!("{}",EMPTY);
    Ok(())
}

pub fn accmain(accus: &str, pa: &str) -> io::Result<()> {
    let mut tokenlo=0;
    let mut file = OpenOptions::new()
    .read(true)
    .write(true)
    .open("localdataccs.txt")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut lines: Vec<&str> = contents.lines().collect();
    for (i, line) in lines.iter().enumerate() {
        let uncrili = xor_crypt(b"9i2a9jr66d5e4f8h", line.as_bytes());
        let ununcrili = std::str::from_utf8(&uncrili).unwrap();
        let parts: Vec<&str> = ununcrili.split(";").collect();
        if parts.len() > 0 && parts[0] == accus && parts[1] == pa {
            tokenlo=1; 
            break;          
        }}
    if tokenlo==1{
    ////////////////////////////////////////
    //////////////////////ACC OPERATIONS///
    print!("\x1B[2J\x1B[1;1H");
    println!("{LOWES}");
    println!("{STACK}");
    println!("{STACK}\n|::::::::::::::::::::  R·Sparsystem·S  .-V0.1 :::::::::::::::::::::|\n{SLICE}");
    println!("|   [ ] {} ] {}", gs("withdraw").unwrap_or_default(), gs("interest").unwrap_or_default());println!("{EMPTY}");
    println!("|   [2] {} ] {}", gs("deposit").unwrap_or_default(), gs("stockmarket").unwrap_or_default());println!("{EMPTY}");
    println!("|   [ ] {} ] {}", gs("transfer").unwrap_or_default(), gs("loan").unwrap_or_default());println!("{EMPTY}");
    println!("|   [ ] {} ] {}", gs("jackpot").unwrap_or_default(), gs("lottery").unwrap_or_default());
    println!("{LOWSQ}");
    print!("|{}: ", gs("opcode").unwrap_or_default());io::stdout().flush().unwrap();
    let mut input = String::new();
    let _ =io::stdin().read_line(&mut input);
    let mode=input.trim();
    print!("\x1B[2J\x1B[1;1H");
    match mode{ 

        "2"=>{deposit(accus);}
        _=>{
        println!("|{}", gs("resrequest").unwrap_or_default());
        println!("{STACK}");
        }
}
    //////////////////////ACC OPERATIONS///
    //////////////////////////////////////
    }
    let mut new_contents = String::new();
    for (i, line) in lines.iter().enumerate() {
        new_contents.push_str(line);
        if i < lines.len() - 1 {
            new_contents.push_str("\n");
        }}
    std::fs::write("localdataccs.txt", new_contents)?;
    Ok(())
}