const STACK:&str="|::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|";
const SLICE:&str="|------------------------------------------------------------------|";
const EMPTY:&str="|                                                                  |";
const LOWES:&str="____________________________________________________________________";
const LOWSQ:&str="|__________________________________________________________________|";
use dialoguer::{Password, theme::ColorfulTheme};
mod sparsystems;

use std::env;
use reqwest::header::{CONTENT_TYPE, AUTHORIZATION};
use reqwest::Response;
use reqwest::Client;
use std::error::Error;
use std::thread;
use std::time::Duration;
use std::fs::OpenOptions;
use std::io::{self, Read, Seek, Write};

fn xor_crypt(key: &[u8], data: &[u8]) -> Vec<u8> {
    data.iter().zip(key.iter().cycle()).map(|(d, k)| d ^ k)
    /*.filter(|&c| c.is_ascii_graphic() || c.is_ascii_whitespace()).filter(|&c| c != b'\n')*/
    .collect()}

fn dataparser()-> Result<(), Box<dyn Error>> { //TODO Unfinished Implementation.- WIP
    let oci_auth_token = env::var("#<MG-[I4y<U.[3vL:X;l")?;
    let oci_region = env::var("MAD")?;
    let oci_namespace = env::var("axqkwgefg9cc")?;
    let oci_bucket_name = env::var("bucket-20240430-1149")?;
    let oci_object_name = "dataccs.txt";
    let new_object_content = "UPDATED.--.--.--";
    let object_storage_url = format!(
        "https://objectstorage.{}.oraclecloud.com/n/{}/b/{}/o/{}",
        oci_region, oci_namespace, oci_bucket_name, oci_object_name
    );
    let client = Client::new();
    let response = client
        .put(&object_storage_url)
        .header(CONTENT_TYPE, "text/plain")
        .header(AUTHORIZATION, format!("Bearer {}", oci_auth_token))
        .body(new_object_content)
        .send();
    thread::sleep(Duration::from_secs(1));
    Ok(())
}

fn accm(accus:&str,pa:&str) -> io::Result<()> {
    let mut file = OpenOptions::new()
    .read(true)
    .write(true)
    .open("localdataccs.txt")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
        let lines: Vec<&str> = contents.split('\n').collect();
        if lines[0].trim().is_empty() {contents = lines.iter().skip(1).cloned().collect::<Vec<_>>().join("\n"); }
    let datatowrite=&format!("{};{};0;",accus,pa);
    let encdata = xor_crypt(b"9i2a9jr66d5e4f8h", datatowrite.as_bytes());
    if encdata.iter().any(|&c| c == b'\n') {
        eprintln!("ERROR: Encrypted data contains undesired character '\\n'");
        std::process::exit(0);//return Err(io::Error::new(io::ErrorKind::Other, "Undesired character found"));
    }
    contents.push_str(&format!("\n{}",std::str::from_utf8(&encdata).unwrap()));
    file.seek(io::SeekFrom::Start(0))?;
    file.write_all(contents.as_bytes())?;
    Ok(())
}
fn accd(accus:&str,pa:&str) -> io::Result<()> {
    let mut file = OpenOptions::new()
    .read(true)
    .write(true)
    .open("localdataccs.txt")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut lines: Vec<&str> = contents.lines().collect();
    for (i, line) in lines.iter().enumerate() {
        let uncrili = xor_crypt(b"9i2a9jr66d5e4f8h", line.as_bytes());
        let ununcrili = std::str::from_utf8(&uncrili).unwrap();
        let parts: Vec<&str> = ununcrili.split(";").collect();
        if parts.len() > 0 && parts[0] == accus && parts[1] == pa {
            lines.remove(i); //DO NOT SHOW THE USER WHETER IT WAS SUCESSFUL;
            break;          //So an undesired user do not keep trying with other's account
        }}
    let mut new_contents = String::new();
    for (i, line) in lines.iter().enumerate() {
        new_contents.push_str(line);
        if i < lines.len() - 1 {
            new_contents.push_str("\n");
        }}
    std::fs::write("localdataccs.txt", new_contents)?;
    Ok(())
}

fn checkus(accus:&str) -> io::Result<bool> {
    let mut file = OpenOptions::new()
    .read(true)
    .write(true)
    .open("localdataccs.txt")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut repeated = false;
    for line in contents.lines() {
        let uncrili=xor_crypt(b"9i2a9jr66d5e4f8h", line.as_bytes());
        let ununcrili=std::str::from_utf8(&uncrili).unwrap();
        let parts: Vec<&str> = ununcrili.split(";").collect();
        if parts.len() > 0 && parts[0] == accus {
            repeated = true;
            break;}}
    Ok(repeated)
}
  /////////////////////////////////
 /**/const LANGUAGE:&str="sv";////Available in "en" ENGLISH && "sv" SVENSKA
/////////////////////////////////
fn gs(key: &str) -> Option<&str> {
    let strings_file = match LANGUAGE {
        "en" => include_str!("strings_en.txt"),
        "sv" => include_str!("strings_sv.txt"),
        _ => panic!("Unsupported language"),
    };
    let strings: Vec<(&str, &str)> = strings_file
        .lines()
        .filter_map(|line| {
            let parts: Vec<&str> = line.splitn(2, '=').map(|s| s.trim()).collect();
            if parts.len() == 2 {
                Some((parts[0], parts[1]))
            } else {None}
        })
        .collect();
    strings.iter().find_map(|(k, v)| if *k == key { Some(*v) } else { None })
}

fn logsy(){
    println!("|{}", gs("modelog").unwrap_or_default());
    println!("{STACK}");println!("{STACK}");
    print!("|{}: ", gs("accusrn").unwrap_or_default());io::stdout().flush().unwrap();
    let mut accus = String::new();let _ =io::stdin().read_line(&mut accus);accus.pop();
    match checkus(&accus) {
        Ok(true) => {
            println!("|{accus} {}", gs("ava").unwrap_or_default());io::stdout().flush().unwrap();
            let mut _paone = Password::with_theme(&ColorfulTheme::default())
            .with_prompt(&format!("{}: ", gs("passnat").unwrap_or_default()))
            .interact().unwrap();
            sparsystems::accmain(&accus,&_paone);
        }
        Ok(false) => {println!("|{accus} {};\n{}", gs("usrnotfounda").unwrap_or_default(), gs("usrnotfoundc").unwrap_or_default());}
        Err(err)  => {println!("|{accus} {};\n{}", gs("usrnotfounda").unwrap_or_default(), gs("usrnotfoundc").unwrap_or_default());}}
}

fn main(){print!("\x1B[2J\x1B[1;1H");
    let _ = dataparser();
    //HEADER
    println!("{LOWES}");
    println!("{}", gs("firststat").unwrap_or_default());
    println!("{STACK}\n|::::::::::::::::::::  R·Sparsystem·S  .-V0.1 :::::::::::::::::::::|\n{SLICE}");
    println!("|   [0] {}1] {}", gs("newac").unwrap_or_default(), gs("sharpri").unwrap_or_default());println!("{EMPTY}");
    println!("|   [2] {}3] {}", gs("logsy").unwrap_or_default(), gs("benlo").unwrap_or_default());println!("{EMPTY}");
    println!("|   [4] {} ] {}", gs("resac").unwrap_or_default(), gs("chrul").unwrap_or_default());println!("{EMPTY}");
    println!("|   [6] {}7] {}", gs("delac").unwrap_or_default(), gs("deldat").unwrap_or_default());
    println!("{LOWSQ}");
    print!("|{}: ", gs("opcode").unwrap_or_default());io::stdout().flush().unwrap();
    let mut input = String::new();
    let _ =io::stdin().read_line(&mut input);
    let mode=input.trim();
    print!("\x1B[2J\x1B[1;1H");
    match mode{ 

        "0"=>{create();}
        "1"=>{visualization(1);}
        "2"=>{logsy();}
        "3"=>{visualization(2);}
        "4"=>{deletereset(2);}
        "6"=>{deletereset(1);}
        "7"=>{delall();}
        _=>{
        println!("|{}", gs("resrequest").unwrap_or_default());
        println!("{STACK}");
        }
}}//https://blogs.oracle.com/timesten/post/using-rust-with-oracle-databases
  //https://docs.rs/oracle/latest/oracle/

use chrono::{Utc, Datelike};
fn visualization(mo:usize)-> io::Result<()>{
    //SHOW LAST 12 MONTHS EVOLUTION
    let mut file = OpenOptions::new().read(true).write(true).open("sysdat.txt")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut values = vec![0; 12];let mut number_of = 0;
    for line in contents.lines() {
        let parts: Vec<&str> = line.split(";").collect();
        if parts.len() > 0 && parts[0] == "M" {
            if let Ok(value) = parts[mo].parse::<i32>() {
                values[number_of] = value;}
            number_of += 1;
            }}
    let current_date = Utc::today();let current_month = current_date.month() as usize;let sum: i32 = values.iter().sum();
    print!("{}.-{}-.SEK\n", gs(if mo==1 { "sharprispec" } else { "netinco" }).unwrap_or_default(),if mo==1 {values[current_month-1]}else{sum});
    let min_value = *values.iter().min().unwrap();let max_value = *values.iter().max().unwrap();
    let adjusted_values: Vec<_> = values.iter().map(|&x| x - (min_value/2)).collect();
    let max_height = 8;
    let scaling_factor = 8.0 / *adjusted_values.iter().max().unwrap() as f64;
    let scaled_values: Vec<_> = adjusted_values.iter().map(|&x| (x as f64 * scaling_factor) as usize).collect();
    for height in 0..max_height {
        for value in &scaled_values {
            if height < max_height - value {
                print!("  ");
            } else {
                print!("██");
            }
        }
        if height==0{print!(".-{}-.SEK",max_value);}
        if height==(max_height-1){print!(".-{}-.SEK",min_value/2);}
        print!("\n");}
        let month_string = format!("{} ", gs("months").unwrap_or_default());
        println!("{}{}", &month_string[(2*current_month)..],&month_string[..(2*current_month)]);
        Ok(())
}

fn create(){
    //THE FIRST ACCOUNT CREATED IS THE ADMIN ACCOUNT
    println!("|{}", gs("modeacre").unwrap_or_default());
    println!("{STACK}");println!("{STACK}");
    print!("|{}: ", gs("accusrn").unwrap_or_default());io::stdout().flush().unwrap();
    let mut accus = String::new();let _ =io::stdin().read_line(&mut accus);accus.pop();
    match checkus(&accus) {
        Ok(true) => {println!("|{accus} {};\n{}", gs("usrnotfounda").unwrap_or_default(), gs("usrnotfoundb").unwrap_or_default());}
        Ok(false) => {
            println!("|{accus} {}", gs("ava").unwrap_or_default());io::stdout().flush().unwrap();
            let mut _paone = Password::with_theme(&ColorfulTheme::default())
            .with_prompt(&format!("{};\n  {}: ", gs("passa").unwrap_or_default(), gs("passb").unwrap_or_default()))
            .interact().unwrap();
            let mut _patwo = Password::with_theme(&ColorfulTheme::default())
            .with_prompt(&format!("{}: ", gs("passre").unwrap_or_default()))
            .interact().unwrap();
            if _paone==_patwo{accm(&accus,&_patwo);
            println!("{}.-", gs("acccreated").unwrap_or_default());} else {
            println!(":::{}:::", gs("passnotmatch").unwrap_or_default());}
        }Err(err) => {println!("|{accus} {};\n{}", gs("usrnotfounda").unwrap_or_default(), gs("usrnotfoundb").unwrap_or_default());}}
}

fn deletereset(mo:usize){
    println!("|{}", gs(if mo==1 {"modeadel"} else {"modeares"}).unwrap_or_default());
    println!("{STACK}");println!("{STACK}");
    print!("|{}: ", gs("accusrn").unwrap_or_default());io::stdout().flush().unwrap();
    let mut accus = String::new();let _ =io::stdin().read_line(&mut accus);accus.pop();
    match checkus(&accus) {
        Ok(true) => {
            println!("|{accus} {}", gs(if mo==1 {"coulderased"} else {"couldreset"}).unwrap_or_default());io::stdout().flush().unwrap();
            let mut _paone = Password::with_theme(&ColorfulTheme::default())
            .with_prompt(&format!("{}: ", gs(if mo==1 {"passc"} else {"passrst"}).unwrap_or_default()))
            .interact().unwrap();
            let mut _patwo = Password::with_theme(&ColorfulTheme::default())
            .with_prompt(&format!("{}: ", gs("passre").unwrap_or_default()))
            .interact().unwrap();
            if _paone==_patwo{if mo==1 {accd(&accus,&_patwo);} else {accd(&accus,&_patwo);accm(&accus,&_patwo);}} else {
            println!(":::{}:::", gs("passnotmatch").unwrap_or_default());}
        }
        Ok(false) => {println!("|{accus} {};\n{}", gs("usrnotfounda").unwrap_or_default(), gs("usrnotfoundc").unwrap_or_default());}
        Err(err)  => {println!("|{accus} {};\n{}", gs("usrnotfounda").unwrap_or_default(), gs("usrnotfoundc").unwrap_or_default());}}
}

fn sysreset()-> io::Result<()> {
    let mut fileusrs = OpenOptions::new().write(true).truncate(true).open("localdataccs.txt")?;
    let mut file = OpenOptions::new().write(true).truncate(true).open("sysdat.txt")?;
    let mut datatowrite =("0;SEK;0\nM;0;0\nM;0;0\nM;0;0\nM;0;0\nM;0;0\nM;0;0\nM;0;0\nM;0;0\nM;0;0\nM;0;0\nM;0;0\nM;0;0");
    file.seek(io::SeekFrom::Start(0))?;
    file.write_all(datatowrite.as_bytes())?;
    Ok(())
}
fn delall()-> io::Result<()>{
    println!("|{}", gs("modesysres").unwrap_or_default());
    println!("{STACK}");println!("{STACK}");
    
    let mut _paone = Password::with_theme(&ColorfulTheme::default())
    .with_prompt(&format!("{}: ", gs("adpas").unwrap_or_default()))
    .interact().unwrap();
    let mut padmin="";
        let mut file = OpenOptions::new().read(true).write(true).open("localdataccs.txt")?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;
        let uncrili=xor_crypt(b"9i2a9jr66d5e4f8h", contents.as_bytes());
        let ununcrili=std::str::from_utf8(&uncrili).unwrap();
        let parts: Vec<&str> = ununcrili.split(";").collect();
        //MUST BE MORE THAN ONE ACCOUNT IN THE SYSTEM TO ERASE IT
        if parts.len() > 0 {padmin=parts[1];} else {padmin=""}
        
    if _paone==padmin{sysreset();} else {
    println!(":::{}:::", gs("passnotmatch").unwrap_or_default());}
    Ok(())
}